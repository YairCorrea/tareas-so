## Tareas - Sistemas Operativos - 2020-1

![Build Status](https://gitlab.com/SistemasOperativos-Ciencias-UNAM/2020-1/tareas-so/badges/master/build.svg)

En este repositorio estaremos manejando las tareas de la materia de [sistemas operativos][url-sistemas_operativos-gitlab] que se imparte en la [Facultad de Ciencias, UNAM][url-sistemas_operativos-fciencias] en el semestre 2020-1.

[url-sistemas_operativos-gitlab]: https://SistemasOperativos-Ciencias-UNAM.gitlab.io/ "Página en GitLab"
[url-sistemas_operativos-fciencias]: "http://www.fciencias.unam.mx/docencia/horarios/20201/1556/713" "Sistemas Operativos 2020-1"

